package databases.connexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import dao.DAOFactory;
import dao.DAOFactory.TypePersistance;
import dao.exception.PKException;
import dao.exception.ValidationException;
import databases.uri.Databases;
import model.Propriete;
import model.TypeComposant;

public class Test2 {

	public static void main(String[] args) {

		try {

			ConnexionSingleton.setInfoConnexion(
					new ConnexionFromFile("./ressources/connexion_composantsTest.properties", Databases.FIREBIRD));

			Connection connection = ConnexionSingleton.getConnexion();

			DAOFactory fbFactory = DAOFactory.getDAOFactory(TypePersistance.FIREBIRD, connection);

			int nbTypeComposant = fbFactory.getTypeComposantDAO().count();
			System.out.println("Nbr de types de composant: " + nbTypeComposant);
			Optional<TypeComposant> oRes = fbFactory.getTypeComposantDAO().getFromID("RES");
			oRes.ifPresentOrElse((c) -> System.out.println(c), () -> {
				System.out.println("N'existe pas");
			});
			System.out.println(fbFactory.getProprieteDAO().getListeFromTypeComposant("RES"));
			Optional<Propriete> op=fbFactory.getProprieteDAO().getFromID("Ohms");
			op.ifPresentOrElse((c) -> System.out.println(c), () -> {
				System.out.println("N'existe pas");
			});
			
			TypeComposant typeRes=new TypeComposant("RES2", "ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppkkk");
			fbFactory.getTypeComposantDAO().insert(typeRes);
			
			ConnexionSingleton.liberationConnexion();
		} catch (PersistanceException e) {
			System.out.println("Problème");
		} catch (Exception e) {
			if (e instanceof ValidationException exc) {
				System.out.println(exc.getMessage()+" Champ: "+exc.getChamp());
			}else 
				if (e instanceof PKException exc) {
				System.out.println(exc.getMessage()+" Champ: "+exc.getId());
			}
		}

	}

}










