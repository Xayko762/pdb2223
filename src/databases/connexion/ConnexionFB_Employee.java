package databases.connexion;

import java.util.Properties;

import databases.uri.Databases;

public class ConnexionFB_Employee implements IConnexionInfos {

	@Override
	public Properties getProperties() {
		Properties props = new Properties();
		props.setProperty("user", "SYSDBA");
		props.setProperty("password", "masterkey");
		props.setProperty("encoding", "NONE");
		props.setProperty("url", Databases.FIREBIRD.buildServeurURL("employee", "localhost"));
		props.setProperty("autoCommit", "false");
		return props;
	}

}
