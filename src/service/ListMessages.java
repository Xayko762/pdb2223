package service;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import lombok.Data;

/**
 * Lors d'une publication, on enverra un objet ListMessage qui contiendra tous
 * les évènements qui se sont produits en spécifiant la classe concernée, l'objet modifié et le type
 * d'opération
 * 
 * @author Didier
 *
 */
public class ListMessages {

	/**
	 * 
	 * @author Didier Type des objets envoyés
	 */
	public enum Classe {
		COMPOSANT, INFORMATION, TYPE_COMPOSANT, PROPRIETE
	}

	/**
	 * 
	 * @author Didier un évènement sur un objet avec l'objet , l'opération sur cet
	 *         objet
	 */
	@Data
	public static class Evenement {
		private final TypeOperation op;
		private final Object element;
	}

	/**
	 * La liste des opérations effectuées
	 */
	private Map<Classe, Evenement> messages;

	/**
	 * Crée une map de messages (un maximum par classe)
	 */
	public ListMessages(Map<Classe, Evenement> messages) {
		this.messages = messages;
	}

	/**
	 * retourne une liste non modifiable des évènements
	 * 
	 * @return liste non modifiable des évènements
	 */
	public Collection<Evenement> getListe() {

		return Collections.unmodifiableCollection(messages.values());
	}

	/**
	 * Permet de savoir si la liste contient un évènement sur une classe
	 * 
	 * @param classe
	 * @return vrai s'il existe un évènement sur cette classe
	 */
	public boolean contientEventFromClasse(Classe classe) {
		return messages.containsKey(classe);
	}

	/**
	 * Renvoie l'évènement lié à un type de classe
	 * 
	 * @param classe
	 * @return un optional d'évènement
	 */
	public Optional<Evenement> getEventFromClasse(Classe classe) {
		return Optional.ofNullable(messages.get(classe));
	}

}
