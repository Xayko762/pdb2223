package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Flow.Subscriber;

import dao.DAOFactory;
import dao.exception.ComposantException;
import model.Composant;
import model.Information;
import model.TypeComposant;
import service.ListMessages.Classe;
import service.ListMessages.Evenement;

public class Facade {
	// Accès aux dao
	private DAOFactory factory;
	// Le publisher
	private PublisherEvent publisher;

	// Fake liste de Composants
	Composant r1 = new Composant(1, "R33", 34, 0.1, "B0101", "RES");
	Composant c1 = new Composant(2, "C47", 10, 0.3, "B0115", "COND");
	Map<String, Composant> liste = new HashMap<>();

	private List<TypeComposant> typeC;

	/**
	 * @param factory
	 */
	public Facade(DAOFactory factory) {
		// La fabrique pour accéder aux DAOs
		this.factory = factory;
		// Le Publisher pour publier les évènements
		publisher = new PublisherEvent();

		// typeC = factory.getTypeComposantDAO().getListe(null);
		// Fake
		liste.put(r1.getRef(), r1);
		liste.put(c1.getRef(), c1);
		typeC = new ArrayList<>();
		typeC.add(new TypeComposant("RES", "Résistance"));
		typeC.add(new TypeComposant("COND", "Condensateur"));

	}

	/**
	 * Obtenir un composant via son ID
	 * 
	 * @param id
	 * @return un optional de composant
	 */
	public Optional<Composant> getComposant(Integer id) {

		return liste.values().stream().filter((c) -> c.getId().equals(id)).findFirst();
		// factory.getComposantDAO().getFromID(id);
	}

	/**
	 * Obtenir un composant via sa référence
	 * 
	 * @param ref
	 * @return un optional de composant
	 */
	public Optional<Composant> getComposant(String ref) {
		Composant res = liste.get(ref);
		return Optional.ofNullable(res);
		// return factory.getComposantDAO().getFromRef(ref);
	}

	/**
	 * Crée un nouveau composant
	 * 
	 * @param cmp le composant avec un ID à null
	 * @return le composant avec un ID généré
	 * @throws Exception
	 */
	public Composant createComposant(Composant cmp) throws ComposantException {
		assert cmp.getId() == null : "L'identifiant doit être vide";
		try {
			if (!liste.containsKey(cmp.getRef())) {
				// fake
				int id = liste.size() + 1;
				cmp.setId(id);
				liste.put(cmp.getRef(), cmp);
			}
			// factory.getComposantDAO().insert(cmp);

			// ajoute ses propriétés par défaut
			// factory.getInformationDAO().createDefaultProperties(cmp);

			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.COMPOSANT, new Evenement(TypeOperation.INSERT, cmp));
			publisher.submit(new ListMessages(messages));

		} catch (Exception e) {
			// dispatch l'exception comme un type d'exception: ComposantException
			if (e instanceof ComposantException exc) {
				throw exc;
			}
			e.printStackTrace();
		}
		return cmp;
	}

	/**
	 * Mis à jour d'un composant
	 * 
	 * @param cmp le composant
	 * @throws Exception
	 */
	public void updateComposant(Composant cmp) throws ComposantException {
		boolean ok;
		try {
			ok = true;
			// factory.getComposantDAO().update(cmp);
			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.COMPOSANT, new Evenement(TypeOperation.UPDATE, cmp));
			publisher.submit(new ListMessages(messages));
			
		} catch (Exception e) {
			if (e instanceof ComposantException exc) {
				throw exc;
			}
			e.printStackTrace();
		}
	}

	/**
	 * Retourne la liste des composants sans charger ses propriétés
	 * 
	 * @return liste des composants
	 */
	public List<Composant> getListe() {
		return new ArrayList<>(liste.values());// factory.getComposantDAO().getListe("");
	}

	/**
	 * Retourne la liste de toutes les références des composant
	 * 
	 * @return liste de références
	 */
	public List<String> getListeRefComposant() {
		return liste.values().stream().map((c) -> {
			return c.getTypeCmp();
		}).distinct().toList();
		// return factory.getComposantDAO().getListeRef();
	}

	/**
	 * Supprime un composant
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void deleteComposant(Integer id) throws ComposantException {
		try {
			// supprime le composant et ses informations
			Optional<Composant> oCmp = liste.values().stream().filter(c -> c.getId().equals(id)).findFirst();
			oCmp.ifPresent((c) -> liste.remove(c.getRef()));
			if (oCmp.isPresent()) {
				// publie l'évènement
				Map<Classe, Evenement> messages = new HashMap<>();
				messages.put(Classe.COMPOSANT, new Evenement(TypeOperation.DELETE, oCmp.get()));
				publisher.submit(new ListMessages(messages));
			}
		} catch (Exception e) {
			if (e instanceof ComposantException exc) {
				throw exc;
			}
			e.printStackTrace();
		}
	}

	public List<Information> getListePropriete(Integer composantId) {
		return null;// factory.getInformationDAO().getListeFromComposant(composantId);
	}

	public void updateInformation(Information information) throws ComposantException {
		try {
			// factory.getInformationDAO().update(information);
		} catch (Exception e) {
			if (e instanceof ComposantException exc) {
				throw exc;
			}
			e.printStackTrace();

		}
	}

	public Integer getNbComposants() {
		return 2;// factory.getComposantDAO().count();
	}

	/*
	 * ******* TypeComposant et Propriétés ********
	 * 
	 */

	public List<TypeComposant> getListeTypesComposant() {
		return new ArrayList<TypeComposant>(typeC);
	}

	/**
	 * Retourne un type de composant avec ses propriétés triée Oblig et Nom
	 * 
	 * @param typeComposant
	 * @return Optional de typeComposant avec ses propriétés
	 */
	public Optional<TypeComposant> getTypeComposant(String idTypeComposant) {
		return factory.getTypeComposantDAO().getFromID(idTypeComposant);
	}

	/**
	 * Ajout un nouveau type de composant avec ses propriétés
	 * 
	 * @param typeComposant
	 * @throws ComposantException
	 */
	public void ajoutTypeComposant(TypeComposant typeComposant) throws ComposantException {
		try {
			TypeComposant typeC = factory.getTypeComposantDAO().insert(typeComposant);
			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.TYPE_COMPOSANT, new Evenement(TypeOperation.INSERT, typeC));
			publisher.submit(new ListMessages(messages));
		} catch (Exception e) {
			if (e instanceof ComposantException exc) {
				throw exc;
			}
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet de s'abonner aux évènements sur le modèle
	 */
	public void addObserver(Subscriber<ListMessages> obs) {
		publisher.addObserver(obs);
	}

}
