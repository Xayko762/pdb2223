package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import model.TypeComposant;

@Slf4j
public class SQLTypeComposantDao implements ITypeComposantDao {
	private static final String SQL_COUNT = """
			SELECT COUNT(*) FROM TTYPE
			""";

	private static final String SQL_FromID = """
			SELECT CODE_TYP , NOM_TYP FROM TTYPE WHERE CODE_TYP = ?
			""";

	private static final String SQL_INSERT = """
			INSERT INTO TTYPE(CODE_TYP , NOM_TYP)
			VALUES (? , ?)
			""";

	private DAOFactory fabrique;

	private Connection connection;

	/**
	 * @param fabrique
	 */
	public SQLTypeComposantDao(DAOFactory fabrique) {
		this.fabrique = fabrique;
		this.connection = fabrique.getConnection();
	}

	@Override
	public Optional<TypeComposant> getFromID(String id) {
		TypeComposant obj = null;
		try (var q = connection.prepareStatement(SQL_FromID)) {
			q.setString(1, id);
			ResultSet rs = q.executeQuery();
			if (rs.next())
				obj = new TypeComposant(rs.getString(1), rs.getString(2),
						fabrique.getProprieteDAO().getListeFromTypeComposant(id));

		} catch (SQLException e) {
			log.error("Problème GetFromID " + e.getMessage());
		}
		return Optional.ofNullable(obj);
	}

	@Override
	public List<TypeComposant> getListe(String regExpr) {
		// TODO Auto-generated method stub
		return ITypeComposantDao.super.getListe(regExpr);
	}

	@Override
	public TypeComposant insert(TypeComposant obj) throws Exception {

		try (var q = connection.prepareStatement(SQL_INSERT)) {
			q.setString(1, obj.getCode().trim().toUpperCase());
			q.setString(2, obj.getNom());

			q.executeUpdate();
			if (!connection.getAutoCommit())
				connection.commit();
			log.info("Insertion d'un Type de composant :" + obj.getCode());
		} catch (SQLException e) {
			if (!connection.getAutoCommit())
				connection.rollback();
			log.error("Problème d'insertion d'un type de composant: " + e.getMessage());
			// transformer l'exception SQL en une exception Composant
			String detail;
			if (e.getMessage().contains("NOM_TYP"))
				detail = "nom";
			else
				detail = "code";
			fabrique.dispatchException(e, detail);
		}

		return obj;
	}

	@Override
	public int count() {
		int res = 0;
		try (var q1 = connection.createStatement()) {
			ResultSet rs = q1.executeQuery(SQL_COUNT);
			if (rs.next())
				res = rs.getInt(1);

		} catch (SQLException e) {
			res = -1;
			log.error(" Erreur lors d'un count sur TType " + e.getMessage());
		}

		return res;
	}

}
