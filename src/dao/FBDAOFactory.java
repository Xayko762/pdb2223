package dao;

import java.sql.Connection;
import java.sql.SQLException;

import dao.exception.ComposantException;
import dao.exception.PKException;
import dao.exception.ValidationException;

/**
 * Fabrique concrète pour FB
 * 
 * @author Didier
 *
 */
//@Slf4j
public class FBDAOFactory extends DAOFactory {

	private Connection connection;

	private ITypeComposantDao composantDAO = null;
	private IProprieteDao proprieteDAO = null;

	/**
	 * @param connection
	 */
	public FBDAOFactory(Connection connection) {
		this.connection = connection;
	}

	@Override
	public ITypeComposantDao getTypeComposantDAO() {
		if (composantDAO == null)
			composantDAO = new SQLTypeComposantDao(this);
		return composantDAO;
	}

	@Override
	public IProprieteDao getProprieteDAO() {
		if (proprieteDAO == null)
			proprieteDAO = new SQLProprieteDao(this);
		return proprieteDAO;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}

	@Override
	protected void dispatchException(Exception e, String detail) throws ComposantException {
		if (e instanceof SQLException exc) {
			switch (exc.getErrorCode()) {
			case 335544665 -> throw new PKException("Problème d'identifiant", detail);
			case 335544347, 335544914 -> throw new ValidationException("Problème de validation: ", detail);
			default -> throw new ComposantException("Erreur...?");
			}
		}
		throw new ComposantException("Problème x ?");

	}

}
