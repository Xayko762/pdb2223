package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import model.Propriete;
import model.TypePropriete;

@Slf4j
public class SQLProprieteDao implements IProprieteDao {
	private static final String SQL_GET_FROMID = """
			SELECT a.NOM_PRO, a.TYPE_PRO, a.LISTEUNITE_PRO, a.LISTECHOIX_PRO,
			a.OBLIGATOIRE_PRO,FKTYPE_PRO
			FROM TPROPRIETE a
			WHERE a.NOM_PRO = ?
			""";
	private static final String SQL_FIND_FROM_TYPE = """
			SELECT a.NOM_PRO, a.TYPE_PRO, a.LISTEUNITE_PRO, a.LISTECHOIX_PRO,
			a.OBLIGATOIRE_PRO
			FROM TPROPRIETE a
			WHERE a.FKTYPE_PRO= ?
			ORDER BY a.OBLIGATOIRE_PRO DESC, a.NOM_PRO ASC
			""";

	private DAOFactory fabrique;

	private Connection connection;

	/**
	 * @param fabrique
	 */
	public SQLProprieteDao(DAOFactory fabrique) {
		this.fabrique = fabrique;
		this.connection = fabrique.getConnection();
	}

	@Override
	public Optional<Propriete> getFromID(String id) {
		Propriete obj = null;
		try (var q = connection.prepareStatement(SQL_GET_FROMID)) {
			q.setString(1, id);
			ResultSet rs = q.executeQuery();
			if (rs.next())
				obj = new Propriete(rs.getString("NOM_PRO"), rs.getBoolean("OBLIGATOIRE_PRO"),
						rs.getString("LISTECHOIX_PRO"), rs.getString("LISTEUNITE_PRO"),
						TypePropriete.valueOf(rs.getString("TYPE_PRO")),
						rs.getString("FKTYPE_PRO"));

		} catch (SQLException e) {
			log.error("Problème GetFromID " + e.getMessage());
		}
		return Optional.ofNullable(obj);
	}

	@Override
	public List<Propriete> getListeFromTypeComposant(String typeComposant) {
		List<Propriete> liste = new ArrayList<>();
		try (var q = connection.prepareStatement(SQL_FIND_FROM_TYPE)) {
			q.setString(1, typeComposant);
			ResultSet rs = q.executeQuery();
			while (rs.next())
				liste.add(new Propriete(rs.getString("NOM_PRO"), rs.getBoolean("OBLIGATOIRE_PRO"),
						rs.getString("LISTECHOIX_PRO"), rs.getString("LISTEUNITE_PRO"),
						TypePropriete.valueOf(rs.getString("TYPE_PRO")),typeComposant));

		} catch (SQLException e) {
			log.error("Problème GetListeFromType " + e.getMessage());
		}
		return liste;
	}

}
