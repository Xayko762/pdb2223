package dao;

import java.sql.Connection;

import dao.exception.ComposantException;

/**
 * Fabrique abstraite
 * 
 * @author Didier
 *
 */
public abstract class DAOFactory {
//Type de persistances
	public enum TypePersistance {
		FIREBIRD, H2, ORACLE
	}

//DAO que doit fournir chaque fabrique concrète
	public abstract ITypeComposantDao getTypeComposantDAO();
	public abstract IProprieteDao getProprieteDAO();
	// Méthode statique que génère des fabriques concrètes
	public static DAOFactory getDAOFactory(TypePersistance typeP, Connection connect) {
		switch (typeP) {
		case FIREBIRD:
			return new FBDAOFactory(connect);

		default:
			return null;
		}
	}

// Retourne la connection SQL
	public abstract Connection getConnection();

// Permet de transformer une exception SQL vers une exception Composant
	protected abstract void dispatchException(Exception e,String detail) throws ComposantException;

}
