package dao;

import java.util.List;

import model.Propriete;

public interface IProprieteDao extends IDAO<Propriete, String> {
	List<Propriete> getListeFromTypeComposant(String typeComposant) ;
}
