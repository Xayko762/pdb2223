package dao.exception;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class PKException extends ComposantException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String id;// Id en erreur


	
	public PKException(String message, String id) {
		super(message);
		this.id = id;


		
	}

	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
}
