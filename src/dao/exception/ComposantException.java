package dao.exception;

public class ComposantException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public ComposantException(String message) {
		super(message);
	}

	
}
