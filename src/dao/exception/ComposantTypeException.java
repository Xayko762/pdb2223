package dao.exception;

/**
 * Exception générée lorsque la donnée d'un champ ne respecte pas le type
 * attendu le type attendu sera spécifié au sein de l'exception
 * 
 * @author Didier
 *
 */
public class ComposantTypeException extends ComposantException {
	private static final long serialVersionUID = 1L;
	// Le type que doit respecter le type
	private String type;

	public ComposantTypeException(String message, String type) {
		super(message);
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
