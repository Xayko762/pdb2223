package view.composant;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.MainController;
import dao.exception.ComposantException;
import dao.exception.PKException;
import javafx.collections.FXCollections;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Composant;
import model.TypeComposant;
import service.Facade;

public class VueComposantController implements Initializable {
	// pseudo classe pour les erreurs
	private final PseudoClass errorClass = PseudoClass.getPseudoClass("error");

	@FXML
	private Button btAnnuler;

	@FXML
	private Button btOk;

	@FXML
	private TextField ztId;

	@FXML
	private TextField ztPrix;

	@FXML
	private TextField ztQt;

	@FXML
	private TextField ztRef;

	@FXML
	private TextField ztLocalisation;

	@FXML
	private TextField ztQtUnite;

	@FXML
	private ComboBox<TypeComposant> cbTypeC;

	private Composant cmp;

	private Facade facade;

	private Stage stage;

	private ResourceBundle bundle;

	private boolean nouveau;

	private MainController ctrl;

	@FXML
	void annuler(ActionEvent event) {
		stage.hide();
	}

	@FXML
	void valider(ActionEvent event) {
		// Vérifie la validité des encodages
		boolean bad = checkData();

		if (!bad) {
			try {
				if (!nouveau) {
					cmp.setRef(ztRef.getText());
					cmp.setQt(Integer.parseInt(ztQt.getText()));
					cmp.setPrix(Double.parseDouble(ztPrix.getText()));
					cmp.setLocalisation(ztLocalisation.getText());

					facade.updateComposant(cmp);
					stage.hide();

				} else {

					Composant cmp = facade.createComposant(new Composant(null, ztRef.getText(),
							Integer.parseInt(ztQt.getText()), Double.parseDouble(ztPrix.getText()),
							ztLocalisation.getText(), cbTypeC.getValue().getCode()));
					stage.hide();

				}
			} catch (NumberFormatException e) {
				showErreur("Problème de conversion");
			} catch (ComposantException e) {
				if (e instanceof PKException pkexc)// Si c'est un problème de doublon qui est en cause
					ztRef.pseudoClassStateChanged(errorClass, true);
				showErreur(e.getMessage());
			}
		}
	}

	/**
	 * Vérifie la validité des champs et champ la pseudo-classe "errorClass" en
	 * fonction
	 * 
	 * @return true si les données sont correctes
	 */
	private boolean checkData() {
		boolean bad = false;
		boolean erreur;

		erreur = ztRef.getText().isBlank();
		ztRef.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;

		erreur = ztQt.getText().isBlank();
		ztQt.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;

		erreur = ztPrix.getText().isBlank();
		ztPrix.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;

		erreur = ztLocalisation.getText().isBlank();
		ztLocalisation.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;

		erreur = cbTypeC.getValue() == null;
		cbTypeC.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;
		return bad;
	}

	/**
	 * Crée ou modifie un composant
	 * 
	 * @param facade
	 * @param cmp    le composant ou null pour créer un nouveau
	 * @param stage
	 */
	public void setUp(MainController ctrl, Composant cmp, Stage stage) {
		this.ctrl = ctrl;
		this.cmp = cmp;
		this.facade = ctrl.getFacade();
		this.stage = stage;
		this.nouveau = cmp == null;
		if (cmp != null) {
			// maj de la vue
			ztId.setText(cmp.getId() == null ? "" : cmp.getId().toString());
			ztRef.setText(cmp.getRef() == null ? "" : cmp.getRef());
			ztQt.setText(Integer.toString(cmp.getQt()));
			ztPrix.setText(Double.toString(cmp.getPrix()));
			ztLocalisation.setText(cmp.getLocalisation());
			List<TypeComposant> listeType = facade.getListeTypesComposant();
			Optional<TypeComposant> oTypeCmp = listeType.stream().filter(c -> c.getCode().equals(cmp.getTypeCmp()))
					.findFirst();
			cbTypeC.setItems(FXCollections.observableArrayList());
			oTypeCmp.ifPresent(t -> cbTypeC.setValue(t));
			cbTypeC.setDisable(true);

		} else // nouveau composant
		{
			ztId.setText("");
			ztRef.setText("");
			ztQt.setText("0");
			ztPrix.setText("0.0");

			// charge la liste des types de composant dans la comboBox
			cbTypeC.setItems(FXCollections.observableList(facade.getListeTypesComposant()));
			cbTypeC.setDisable(false);
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle bundle) {
		this.bundle = bundle;
		// Permet d'encoder uniquement un entier
		ztQt.textProperty().addListener((obs, oldV, newV) -> {
			try {
				if (newV.length() > 0)
					Integer.parseInt(newV);
			} catch (Exception e) {
				// si une exception est générée, on remet l'ancienne valeur
				ztQt.setText(oldV);
			}
		});
		
		// Permet d'avoir uniquement un double
		ztPrix.textProperty().addListener((obs, oldV, newV) -> {
			try {
				if (newV.length() > 0)
					Double.parseDouble(newV);
			} catch (Exception e) {
				// si une exception est générée, on remet l'ancienne valeur
				ztPrix.setText(oldV);
			}
		});
	}

	private void showErreur(String message) {
		Alert a = new Alert(AlertType.ERROR, message);
		a.showAndWait();
	}

}
