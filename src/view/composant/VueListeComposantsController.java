package view.composant;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import dao.exception.ComposantException;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import model.Composant;
import service.Facade;
import service.ListMessages;
import service.ListMessages.Classe;
import service.ListMessages.Evenement;
import service.TypeOperation;

@Slf4j
public class VueListeComposantsController implements Initializable, Subscriber<ListMessages> {
	@FXML
	private Button btUpdate;

	@FXML
	private Button btRecharger;

	@FXML
	private TableView<Composant> tblComposants;

	@FXML
	private TableColumn<Composant, Integer> colId;

	@FXML
	private TableColumn<Composant, String> colLoc;

	@FXML
	private TableColumn<Composant, Double> colPrix;

	@FXML
	private TableColumn<Composant, Integer> colQt;

	@FXML
	private TableColumn<Composant, String> colRef;

	@FXML
	private TableColumn<Composant, String> colTypeC;

	@FXML
	void actionRecharger(ActionEvent event) {
		for (int id : mapUpdate.keySet()) {
			Optional<Composant> oc = facade.getComposant(id);
			int index = obListe.indexOf(mapUpdate.get(id));
			obListe.set(index, oc.get());
		}
		mapUpdate.clear();
		update.set(false);
		log.info("RELOAD");
	}

	@FXML
	void actionUpdate(ActionEvent event) {
		// Créé une liste avec les id des composants modifiés
		List<Integer> listeId = new ArrayList<>(mapUpdate.keySet());
		for (int id : listeId) {
			try {
				facade.updateComposant(mapUpdate.get(id));
				mapUpdate.remove(id);
			} catch (ComposantException e) {
				log.error("Can't update composant: " + e.getMessage());
			}
		}
		if (mapUpdate.isEmpty())
			update.set(false);
	}

	private ObservableList<Composant> obListe;

	private Map<Integer, Composant> mapUpdate = new HashMap<>();

	private BooleanProperty update = new SimpleBooleanProperty(false);

	// Pour gérer les évènements de publication
	private Subscription subscription;

	private Facade facade;

	private int indice;// pour l'update dans la gestion de la publication

	public void setUp(Facade facade) {
		this.facade = facade;
		obListe = FXCollections.observableArrayList(facade.getListe());
		tblComposants.setItems(obListe);
		// s'enregistre sur les évènements
		facade.addObserver(this);

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		colId.setCellValueFactory(new PropertyValueFactory<Composant, Integer>("id"));
		colRef.setCellValueFactory(new PropertyValueFactory<Composant, String>("ref"));
		colQt.setCellValueFactory(new PropertyValueFactory<Composant, Integer>("qt"));
		colPrix.setCellValueFactory(new PropertyValueFactory<Composant, Double>("prix"));
		colLoc.setCellValueFactory(new PropertyValueFactory<Composant, String>("localisation"));
		colTypeC.setCellValueFactory(new PropertyValueFactory<Composant, String>("typeCmp"));
		// Associer une composant pour l'édition
		colRef.setCellFactory(TextFieldTableCell.forTableColumn());
		colRef.setOnEditCommit(e -> {
			Composant c = e.getRowValue();
			c.setRef(e.getNewValue());
			mapUpdate.put(c.getId(), c);
			update.set(true);
		});

		colQt.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>() {
			private Integer oldV;

			@Override
			public String toString(Integer i) {
				oldV = i;
				// mémorise l'ancienne valeur
				if (i == null)
					return "";
				return i.toString();
			}

			@Override
			public Integer fromString(String string) {
				Integer i;
				try {
					i = Integer.parseInt(string);
					return i;
				} catch (NumberFormatException e) {
					// Remet l'ancienne valeur en cas de problème
					return oldV != null ? oldV : 0;
				}
			}
		}));
		colQt.setOnEditCommit((e) -> {
			Composant c = e.getRowValue();
			c.setQt(e.getNewValue());
			mapUpdate.put(c.getId(), c);
			update.set(true);
		});

		//
		btUpdate.disableProperty().bind(update.not());
		btRecharger.disableProperty().bind(update.not());
	}

	/****************** GESTION DE LA PUBLICATION ********************/

	// Gestion des publications
	// appellé lors de l'enregistrement on reçoit alors une subscription
	@Override
	public void onSubscribe(Subscription subscription) {
		this.subscription = subscription;
		subscription.request(10);
		log.info("Je suis un écouteur de Composant");
	}

	// Reception des messages
	@Override
	public void onNext(ListMessages messages) {
		// vérifie s'il existe un événement sur un composant
		Optional<Evenement> oEv = messages.getEventFromClasse(Classe.COMPOSANT);
		// récupère le composant s'il existe (et un typecasting automatique java17)
		if (oEv.isPresent() && oEv.get().getElement() instanceof Composant composant) {
			// récupère le type d'opération
			TypeOperation operation = oEv.get().getOp();

			log.info(operation + " sur " + composant);
			switch (operation) {
			case INSERT:
				Platform.runLater(() -> obListe.add(composant));
				break;
			case DELETE:
				Platform.runLater(() -> obListe.remove(composant));
				break;
			case UPDATE: { // recherche le composant dans la liste
				Platform.runLater(() -> {
					indice = -1;
					Optional<Composant> op = obListe.stream().filter((p) -> {
						indice++;
						return p.getId().equals(composant.getId());
					}).findFirst();
					if (op.isPresent())
						;
					obListe.set(indice, composant);
				});
			}
				break;
			default:
				break;
			}
		}
		subscription.request(1);
	}

	@Override
	public void onError(Throwable throwable) {
		log.error("erreur d'abonnement aux évènements");

	}

	@Override
	public void onComplete() {
		log.info(" écouteur de composant: On Complete");
	}

	// Permet d'avoir la "Subscription pour se désabonner"
	public Subscription getSubscription() {
		return subscription;
	}
}
