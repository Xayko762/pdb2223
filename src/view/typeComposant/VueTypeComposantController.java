package view.typeComposant;

import java.net.URL;
import java.util.ResourceBundle;

import controller.MainController;
import dao.exception.ComposantException;
import dao.exception.PKException;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.TypeComposant;

public class VueTypeComposantController implements Initializable {
	// pseudo classe pour les erreurs
	private final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
	@FXML
	private TextField ztCode;

	@FXML
	private TextField ztNom;

	private MainController ctrl;

	private Stage stage;

	@FXML
	void actAjouter(ActionEvent event) {
		boolean bad=checkData();
		if(!bad) {
		TypeComposant tc= new TypeComposant(ztCode.getText(), ztNom.getText());
		try {
			ctrl.getFacade().ajoutTypeComposant(tc);
			stage.close();
		} catch (PKException e) {
				ztCode.pseudoClassStateChanged(errorClass, true);
			
		} catch (ComposantException e) {
			Alert a = new Alert(AlertType.ERROR, e.getMessage());
			a.showAndWait();
		}
		}

	}

	@FXML
	void actAnnuler(ActionEvent event) {
		stage.close();
	}

	@FXML
	void actBrol(ActionEvent event) {

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	public void setUp(MainController ctrl,Stage stage) {
		this.ctrl = ctrl;
		this.stage=stage;
	}
	
	private boolean checkData() {
		boolean bad = false;
		boolean erreur;

		erreur = ztCode.getText().isBlank();
		ztCode.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;

		erreur = ztNom.getText().isBlank();
		ztNom.pseudoClassStateChanged(errorClass, erreur);
		bad = bad || erreur;
		
		return bad;
	}

}
