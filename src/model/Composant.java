package model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
@EqualsAndHashCode(exclude = "details")
@ToString(exclude = "details")
@Data
public class Composant {
	private Integer id;//généré par la BD
	private String ref;
	private int qt;
	private double prix;
	private String localisation;
	private final String typeCmp;
	
	private List<Information> details = new ArrayList<>();

	/**
	 * @param id
	 * @param ref
	 * @param qt
	 * @param prix
	 * @param localisation
	 */
	public Composant(Integer id, String ref, int qt, double prix, String localisation, String typeCmp) {
		this.id = id;
		this.ref = ref;
		this.qt = qt;
		this.prix = prix;
		this.localisation = localisation;
		this.typeCmp = typeCmp;
	}
}
