package model;

import dao.exception.ComposantTypeException;
import lombok.Data;

@Data
public class Information {
	/**
	 * Identifiant d'une information
	 */
	public record IdInformation(Integer composant, String propriete) {
	};

	public IdInformation idInformation;// non modifiable
	private String valeur;
	private String unite;
	private Propriete propriete;

	/**
	 * 
	 * @param idComposant le composant associé
	 * @param valeur      la valeur de la propriété
	 * @param unite       l'unité utilisée
	 * @param propriete   la propriété associée
	 */
	public Information(Integer idComposant, Propriete propriete, String valeur, String unite) {
		this.idInformation = new IdInformation(idComposant, propriete.getNom());
		this.valeur = valeur;
		this.unite = unite;
		this.propriete = propriete;
	}

	/**
	 * change la valeur de la propriété
	 * 
	 * @param valeur
	 * @throws ComposantTypeException si elle ne respecte pas le type de la
	 *                                propriété
	 */
	public void setValeur(String valeur) throws ComposantTypeException {
		this.valeur = valeur;
	}

	@Override
	public String toString() {
		return "Information [idInformation=" + idInformation + ", valeur=" + valeur + ", unite=" + unite + "]";
	}
}
