package model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TypeComposant {
	private String code;
	private String nom;
	private List<Propriete> proprietes = new ArrayList<>();

	/**
	 * @param code
	 * @param nom
	 */
	public TypeComposant(String code, String nom) {
		this.code = code;
		this.nom = nom;
	}
}
