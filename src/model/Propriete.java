package model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Propriete {
	private final String nom;
	private boolean obligatoire;
	private final List<String> listeChoix;
	private final List<String> listeUnite;
	private final TypePropriete typePropriete;
	private final String typeComposant;

	/**
	 * @param nom
	 * @param obligatoire
	 * @param listeChoix
	 * @param listeUnite
	 * @param typePropriete
	 */
	public Propriete(String nom, boolean obligatoire, String listeChoix, String listeUnite, TypePropriete typePropriete,
			String typeComposant) {
		super();
		this.nom = nom;
		this.obligatoire = obligatoire;
		this.typeComposant = typeComposant;

		if (listeChoix == null)
			this.listeChoix = new ArrayList<>();
		else
			this.listeChoix = new ArrayList<>(List.of(listeChoix.split(";")));

		this.listeUnite = listeUnite == null ? new ArrayList<>() : new ArrayList<>(List.of(listeUnite.split(";")));
		this.typePropriete = typePropriete;
	}

	/**
	 * Permet de généré un string avec ";" comme séparateur
	 * 
	 * @return
	 */
	public String getListeUniteStr() {
		return String.join(";", listeUnite);
	}

	/**
	 * Permet de généré un string avec ";" comme séparateur
	 * 
	 * @return
	 */
	public String getListeChoixStr() {
		return String.join(";", listeChoix);
	}

	/**
	 * Permet à partir d'un string et séparateur ";" de mettre à jour la liste
	 * 
	 * @param listeChoix
	 */
	public void setListeChoixStr(String listeChoix) {
		this.listeChoix.clear();
		this.listeChoix.addAll(List.of(listeChoix.split(";")));
	}
	/**
	 * Permet à partir d'un string et séparateur ";" de mettre à jour la liste
	 * 
	 * @param listeUnité
	 */
	public void setListeUniteStr(String listeUnite) {
		this.listeUnite.clear();
		this.listeUnite.addAll(List.of(listeUnite.split(";")));
	}

}
