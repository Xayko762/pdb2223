package controller;

import org.controlsfx.control.ListSelectionView;
import org.controlsfx.control.ToggleSwitch;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Jfx1 extends Application {

	@Override
	public void start(Stage ps) {
		//Conteneur
		BorderPane cp=new BorderPane();
		//Ajout
		Button bt1 = new Button("CENTRE");
		Label lbl1 = new Label("LEFT");
		//Ajout des controls au cp
		cp.setRight(bt1);
		cp.setLeft(lbl1);
		
		ToggleSwitch ts=new ToggleSwitch("on/off");
		
		
		HBox hb=new HBox();
		hb.getChildren().addAll(ts);
		cp.setTop(hb);
		
		ListSelectionView<String> view = new ListSelectionView<>();
        view.getSourceItems()
                .addAll("Katja", "Dirk", "Philip", "Jule", "Armin");

        GridPane pane = new GridPane();
        pane.add(view, 0, 0);
        pane.setAlignment(Pos.CENTER);
        cp.setCenter(pane);
		bt1.setOnAction((e)->{
			view.getSourceItems().add("New");
			view.getTargetItems().forEach((it)->System.out.println(it));
		});
		
		//scene
		Scene scene= new Scene(cp,500,400);
		//Centre tous les composants
		cp.getChildren().stream().forEach(n->BorderPane.setAlignment(n, Pos.CENTER));
				
		//Spécifie un style
		cp.setStyle("""
				-fx-background-color:azure; 
				-fx-border-color:skyblue ;
				-fx-border-width:3
				""");
		//Ajout scene à la stage
		ps.setScene(scene);
		ps.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
