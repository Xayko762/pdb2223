package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ResourceBundle;

import dao.DAOFactory;
import dao.DAOFactory.TypePersistance;
import databases.connexion.ConnexionFromFile;
import databases.connexion.ConnexionSingleton;
import databases.connexion.IConnexionInfos;
import databases.uri.Databases;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import model.Composant;
import service.Facade;
import view.composant.VueComposantController;
import view.composant.VueListeComposantsController;
import view.typeComposant.VueTypeComposantController;

@Slf4j
public class MainController extends Application {
//Facade
	private Facade facade;
//Factory
	private DAOFactory factory;

//MainStage
	private Stage mainStage;

//Lancement de l'application
	@Override
	public void start(Stage mainStage) {
		// mémorise la fénêtre principale
		this.mainStage = mainStage;
		// Connexion à la BD
		try {
			IConnexionInfos info = new ConnexionFromFile("./ressources/connexion_composants.properties",
					Databases.FIREBIRD);
			ConnexionSingleton.setInfoConnexion(info);
			Connection connect = ConnexionSingleton.getConnexion();
			log.info("Connexion établie");
			// Crée la factory Firebird
			factory = DAOFactory.getDAOFactory(TypePersistance.FIREBIRD, connect);
		} catch (Exception e) {
			showErreur("Problème de connexion");
			// Quitte l'application
			Platform.exit();
		}
		// facade
		facade = new Facade(factory);

		BorderPane cp = new BorderPane();
		// Liste de boutons
		VBox leftPane = new VBox();
		Button bt1 = new Button("Affiche Composant");
		leftPane.getChildren().add(bt1);
		// bt1 Affiche composant
		bt1.setOnAction(ev -> {
			showVueComposant(facade, facade.getComposant(1).get());
		});

		// bt2 crée un nouveau composant
		Button bt2 = new Button("Nouveau Composant");
		bt2.setOnAction(ev -> {
			showVueComposant(facade, null);
		});

		leftPane.getChildren().add(bt2);
		// Affiche la liste des composants
		Button bt3 = new Button("Liste Composants");
		bt3.setOnAction(ev -> {
			showVueListeComposant(facade);
		});

		leftPane.getChildren().add(bt3);

// Affiche la liste des composants
		Button bt4 = new Button("Ajout TypeComposant");
		bt4.setOnAction(ev -> {
			showAddTypeComposant();
		});

		leftPane.getChildren().add(bt4);
		cp.setLeft(leftPane);

		Scene scene = new Scene(cp, 500, 400);
		mainStage.setScene(scene);
		mainStage.setTitle("Projet PDB 2223");
		mainStage.show();

	}

	/**
	 * 
	 */
	private void showAddTypeComposant() {
		// bundle
		ResourceBundle bundle;
		// Crée une stage
		Stage stage = new Stage();
		// indique sa stage parent
		stage.initOwner(mainStage);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setX(100);
		stage.setY(50);

		// Crée un loader pour charger la vue FXML
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/typeComposant/VueTypeComposant.fxml"));

		try {
			bundle = ResourceBundle.getBundle("view.typecomposant.bundle.VueTypeComposant");
			loader.setResources(bundle);
			// Obtenir la traduction du titre dans la locale
			stage.setTitle(bundle.getString("titre"));
		} catch (Exception e) {
			log.error("Imposible de charger le buddle pour la vue TypeComposant" + e.getMessage());
			showErreur("Impossible de charger le buddle ");
			stage.setTitle("Vue TypeComposant");
		}
		// Charge la vue à partir du Loader
		// et initialise son contenu en appelant la méthode setUp du controleur
		AnchorPane root;
		try {
			root = loader.load();
			// récupère le ctrl (après l'initialisation)
			VueTypeComposantController ctrl = loader.getController();
			// fourni la fabrique au ctrl pour charger les articles
			ctrl.setUp(this, stage);
			// charge le Pane dans la Stage
			Scene scene = new Scene(root);
			scene.getStylesheets().add("./view/css/composant.css");
			stage.setScene(scene);
			stage.showAndWait();
		} catch (IOException e) {
			log.error("Imposible de charger la vue TypeComposant");
			showErreur("Impossible de charger la vue TypeComposant: " + e.getMessage());
		}
		stage = null;
	}

	/**
	 * Permet de modifier ou créer un nouveau composant
	 * 
	 * @param facade
	 * @param composant le composant avec un id (modif), un composant sans id
	 *                  (créer)
	 */
	private void showVueComposant(Facade facade, Composant composant) {
		// bundle
		ResourceBundle bundle;
		// Crée une stage
		Stage stage = new Stage();
		// indique sa stage parent
		stage.initOwner(mainStage);
		stage.setX(100);
		stage.setY(50);

		// Crée un loader pour charger la vue FXML
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/composant/VueComposant.fxml"));
		try {
			bundle = ResourceBundle.getBundle("view.composant.bundle.VueComposant");
			loader.setResources(bundle);
			// Obtenir la traduction du titre dans la locale
			stage.setTitle(bundle.getString("composant.titre"));
		} catch (Exception e) {
			log.error("Imposible de charger le buddle pour la vue Composant" + e.getMessage());
			showErreur("Impossible de charger le buddle ");
			stage.setTitle("Vue Composant");
		}
		// Charge la vue à partir du Loader
		// et initialise son contenu en appelant la méthode setUp du controleur
		AnchorPane root;
		try {
			root = loader.load();
			// récupère le ctrl (après l'initialisation)
			VueComposantController ctrl = loader.getController();
			// fourni la fabrique au ctrl pour charger les articles
			ctrl.setUp(this, composant, stage);
			// charge le Pane dans la Stage
			Scene scene = new Scene(root);
			scene.getStylesheets().add("./view/css/composant.css");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			log.error("Imposible de charger la vue Composant");
			showErreur("Impossible de charger la vue Composant: " + e.getMessage());
			stage = null;
		}

	}

	/**
	 * Permet d'afficher la liste des composants
	 * 
	 * @param facade
	 */
	private void showVueListeComposant(Facade facade) {

		ResourceBundle bundle;
		// Crée une stage
		Stage stage = new Stage();
		// indique sa stage parent
		stage.initOwner(mainStage);

		stage.setX(150);
		stage.setY(100);

		// Crée un loader pour charger la vue FXML
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/composant/VueListeComposants.fxml"));
		try {
			bundle = ResourceBundle.getBundle("view.composant.bundle.VueListeComposants");

			loader.setResources(bundle);
			// Obtenir la traduction du titre dans la locale
			stage.setTitle(bundle.getString("titre"));
		} catch (Exception e) {
			log.error("Imposible de charger le bundle pour la vue listeComposant" + e.getMessage());
			showErreur("Impossible de charger le bundle ListeComposant");
			stage.setTitle("Vue Liste Composants");
		}
		// Charge la vue à partir du Loader
		// et initialise son contenu en appelant la méthode setUp du controleur
		Pane root;
		try {
			root = loader.load();
			// récupère le ctrl (après l'initialisation)
			VueListeComposantsController ctrl = loader.getController();
			// fourni la fabrique au ctrl pour charger les articles
			ctrl.setUp(facade);
			// charge le Pane dans la Stage
			Scene scene = new Scene(root, 800, 400);
			scene.getStylesheets().add("./view/css/composant.css");
			stage.setScene(scene);
			stage.show();
			// Se désabonne lors de la fermeture
			stage.setOnHidden(e -> {
				ctrl.getSubscription().cancel();
				log.info(" Liste de composants: Désabonnement des évènements ");
			});

		} catch (IOException e) {
			log.error("Imposible de charger la vue ListeComposants");
			showErreur("Impossible de charger la vue ListeComposants: " + e.getMessage());
			stage = null;
		}

	}

	/**
	 * Vue pour afficher les messages d'erreur
	 * 
	 * @param message
	 */
	private void showErreur(String message) {
		Alert a = new Alert(AlertType.ERROR, message);
		a.showAndWait();
	}

	/**
	 * Point d'entrée principal de l'application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	public Facade getFacade() {
		return facade;
	}

}
